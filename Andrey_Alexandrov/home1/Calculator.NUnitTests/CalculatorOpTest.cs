﻿using System;
using NUnit.Framework;

namespace Calculator.NUnitTests
{
    [TestFixture]
    public class CalculatorOpTest
    {
        [Test]
        public void Sum_7Plus4_returns14()
        {
            CalculatorOp obj = new CalculatorOp();
            int a = 7;
            int b = 4;
            int expected = 11;

            int actual = obj.Add(a, b);

            Assert.That(expected, Is.EqualTo(actual));
        }

        [Test]
        public void Op_3mult8_returns30()
        {
            CalculatorOp obj = new CalculatorOp();
            int a = 3;
            int b = 8;
            int expected = 24;

            int actual = obj.Mult(a, b);

            Assert.That(expected, Is.EqualTo(actual));
        }
    }
}
